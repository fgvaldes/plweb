\documentclass[11pt,twoside]{article}
\usepackage{asp2010}

\resetcounters

\bibliographystyle{asp2010}

\markboth{Valdes and Marru}{Marriage of NHPPS and OGCE}

\begin{document}

\title{The Marriage of Mario (NHPPS) and Luigi (OGCE)}
\author{Francisco~Valdes$^1$, Suresh~Marru$^2$}
\affil{
$^1$NOAO Science Data Management, P.O. Box 26732, Tucson, AZ 85732\\
$^2$Pervasive Technology Institue, Indiana University, Bloomington, IN
47408}

\begin{abstract}

Pipeline systems, like the plumbing characters of video game fame, have
their strengths and weaknesses. This is generally due to the different
niches they occupy in the landscape of pipeline systems.  People frequently
discuss the potential interchange of application elements (the modules and
algorithms) between pipeline systems.  However, pipeline systems themselves
are applications in their own right.  This paper describes an interesting
architectural marriage where one pipeline system orchestrates another
to take advantage of the strengths and niches of both.  This
marriage is the result of a (proposed) project between NOAO and Indiana
University to provide a Pipeline, Portal, and Archive (PPA) system for the
WIYN One-Degree Imager (ODI) which is briefly introduced here.

\end{abstract}

\section{Introduction}

The WIYN Observatory is building a forefront mosaic camera with a one-degree
field of view, hence the name One-Degree Imager (ODI), and a pixel scale of
0.1 arcseconds for its 3.5-meter telescope (Jacoby 2002SPIE.4836..217J).
This gigapixel camera means observers will obtain large volumes of data
which most will be unable to store, calibrate, and analyze with their own
resources.  Therefore, Indiana University (IU) and NOAO have partnered to
propose a Pipeline, Portal, and Archive (PPA) system to support this
instrument.  In this contribution we describe the plans and synergies within
this partnership for developing the standard ODI calibration pipeline.

The pipeline framework, arrived at through design and prototype studies, is
a marriage of two pipeline/workflow systems.  The NOAO High Performance
Pipeline System (NHPPS) is good at orchestrating host commands into
multi-process workflows that effectively make use of a dedicated cluster of
multi-core machines.  The Open Gateway Computing Environment (OGCE)
\citep{OGCE} Workflow Suite is designed to wrap command line-driven science
applications and make them into robust, network-accessible services. The
resulting services are orchestrated as workflows moving data and executed as
jobs on computational resources ranging from local workstations to high
performance computational grids and clouds including the TeraGrid
\citep{teragrid}.  For the ODI project we will wrap NHPPS pipeline
applications with OGCE for orchestration into distributed Teragrid
workflows.

Before going into more detail on this marriage we introduce the IU/NOAO
partnership.  The collaborating groups are the Pervasive Technology
Institute (PTI) at IU and the Science Data Management (SDM) group at NOAO.
PTI is dedicated to the development and delivery of innovative information
technology while SDM is a traditional astronomical data management group.
The differences in expertise between these groups is one of the
characteristics of this partnership.  SDM brings experience with pipeline
processing of astronomical image data similar to ODI and managing a
principle investigator and community archive.  PTI brings experience with
the IT problems of data transport, storage, and computations on a large
scale as well as multidisciplinary science gateways.  The blending of
expertise is needed to address the astronomical needs of the ODI users in
working with the larger volume of ODI data than other current NOAO
instruments.

\section{NOAO High Performance Pipeline System (NHPPS)}

The NOAO High Performance Pipeline System (NHPPS), internally code named
"Mario" at NOAO, has been described in previous ADASS proceedings
(see Scott 2007) and in a more detailed paper \citep{PL001}.
Briefly, NHPPS is an event-driven executor of host commands (aka modules),
where any number of these may be executed at the same time.  The commands
are submitted to the host operating system which is where the processing
scheduling and management takes place.  Because many processes are running
in an asynchronous, time-shared manner the compute resources, such as
multiple cores, can be efficiently utilized by the operating system even
when the processes are not threaded or MPI applications.

NOAO uses this framework to build pipeline applications structured as a
number of pipeline services.  A pipeline service is a sequence of (possibly
parallel) steps running on one type of data object and on a single node to
perform a piece of the overall application.  These pipeline services
interact with each other in a workflow.  The workflow can be executed on a
single node or distributed across peer nodes in a dedicated cluster. In NOAO
applications the individual steps in the pipeline services consist primarily
of IRAF-shell scripts calling standard IRAF data processing tools.  One of
the main advantages of NHPPS in these applications is the ability to run
many of these IRAF modules in parallel to fully utilize compute resources.

In a high performance application the pipeline services are deployed on a
set of compute nodes.   The workflow is structured into a hierarchy such
that higher level pipeline services operate on larger data objects and call
multiple instances of lower level services to process smaller data objects
in parallel.  The calling pipeline service waits for the results of the
lower level pipelines.  This type of workflow execution is typically called
hierarchal orchestration and the workflow structure generally follows the
map/reduce paradigms.

To understand the marriage with OGCE, a key point is that the NHPPS
framework basically consists of a server, called the Node Manager, which runs
on each compute node and responds to events by executing modules on
particular dataset objects.  The efficiency occurs because it manages
many processes at the same time so that the operating system can keep the
CPU cores busy.

When NHPPS is configured as a distributed pipeline system, the Node Managers
communicate with each other and manage multiple instances of the same
pipeline or different pipelines on different nodes depending on the
configuration design.  The need to communicate means that the compute
nodes be part of the cluster providing peer-to-peer connections.

\section{Open Gateway Computing Environment (OGCE)}

The Open Gateway Computing Environment (OGCE), dubbed "Luigi" in this
tongue-in-cheek marriage analogy with "Mario", provides science
gateway software.  It includes several middleware components that can be
used by themselves or integrated to provide more comprehensive
solutions. The toolkit wraps command line scientific applications into web
services, deploys and executes them on the TeraGrid or other Grid and
Cloud Infrastructures, and orchestrates them as a workflow applications.  There
are a variety of components and aspects of OGCE with details provided in
\citet{OGCE}.

A key point is that the Grid infrastructure, while providing good features
of uniform access to heterogeneous resources, add some overhead to perform
meta scheduling and unified security.  The overhead is ignorable for long
running applications but significant enough for very small running
applications.  OGCE as a general purpose toolkit abstracts the utilization
of multiple cores on a grid node and hands it over to the application for
efficient threading or other parallelization techniques.  So primary it
launches a single command line application on a grid node or nodes.  The
command may be multi-threaded, an MPI application, a simple shell script,
etc.  The utilization of the node resources, such as multiple cores, resides
in the application. In the cases like astronomical data processing the
application can be a pipeline itself.  When a pipeline is wrapped as a web
service, the OGCE toolkit orchestrates the pipeline applications onto grid
nodes and leaves the details of internal node orchestration to the pipeline
itself.

\section{The Marriage}

The fundamental technical question for the NOAO/IU partners with regards to
the Tier~1 pipeline was how NOAO developed science pipeline software could
be integrated (married) with the IU grid workflow system.  Another goal,
which we only mention here, is to enable observer workflows, called
Tiers~2/3, using the standard data products, pieces of the standard pipeline
and other tools such as IRAF.

Primary considerations were to utilize the NOAO data reduction and pipeline
expertise and to capitalize on the large body of already developed software.
This means use of the IRAF toolkit and NOAO pipeline applications.  This
leads to using IRAF-shell modules; with reuse of modules from similar NOAO
pipeline applications and addition of new modules as appropriate.  It also
means using the same high performance map/reduce workflow design.

A secondary consideration was to have the potential to run the pipeline
application on both the TeraGrid and on a dedicated cluster of machines.
This provides a development framework, a fallback in case of grid
limitations, and increased flexibility for deployment.  This again leads to
using the NOAO pipeline applications model of NHPPS pipeline services.

The first step was to develop an ODI data flow design \citep{PL013} which
identifies the map/reduce strategies and estimates for the number of modules
($\sim 150$) and pipeline services ($\sim 20$) required.  Then the first
integration concept was to use the individual modules in an OGCE workflow.
After studying this option there were a number of problems.  These include
the large number of modules, the heterogeneity of function and parameters,
and the inefficiency in deploying many light-weight modules as grid jobs.

Then came the "epiphany" that NHPPS pipeline services (the collection
of modules that address a particular map or reduce operation)
would satisfy all of the considerations.  The number of services is
relatively small, the services have consistent input and output
parameters, are not too-fine grained, efficiently utilize node
resources, and are easily orchestrated both by the dedicated cluster
configuration of NHPPS and the TeraGrid framework of OGCE.

The main challenge was that a pipeline service uses NHPPS as the execution
framework that glues the modules together efficiently.  Normally the NHPPS
framework is a daemon on the nodes where the pipeline services are deployed.
However, it was realized that with some minor changes a captive NHPPS
framework can be started for one or more pipeline services within a single
host application.  This host application is then what is wrapped as an OGCE
service.

So here is the core of this contribution:  The marriage of NHPPS with
OGCE consists of wrapping NHPPS pipeline services as single host applications.
Because these pipeline services have a standard structure a single basic
wrapper is all that is needed.  This wrapper accepts input parameters
and produces output parameters that OGCE builds services around.
The wrapper also takes care of starting the single node NHPPS framework,
supplying the triggers that start the pipeline, wait for
the pipeline service to complete, and shutdown the framework.

For OGCE, one web service wrapper is created for each of the pipeline
services.  These web services are then orchestrated by OGCE as a Grid
workflow.  OGCE provides the workflow monitoring and collecting and managing
of the pipeline input and output.   In addition it handles the typical
submission and security aspects of a Grid application.

There was one other detail to resolve.  An NHPPS pipeline service implements
the hierarchal orchestration and map/reduce structure by calling other pipelines and waiting for results.
The OGCE workflows must be described by a directed acyclic graph (DAG) which
means there is no "return" to a service.  The solution was to automatically
split pipeline services at their call (map) and return (reduce) steps and
promote this to the OGCE workflow framework as a list of call requests and
the the collection of map results.

An organizational benefit of this design is that the interface, and
resulting division of work, between NOAO and IU is simple and clear.  NOAO
can concentrate on the NHPPS pipelines knowing the wrapper strategy is
simple and clear and IU can concentrate on the map/reduce workflow aspects
with little need to know about the modules and algorithms.

\bibliography{O12_1}

\end{document}
