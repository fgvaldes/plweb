\documentclass{../latex/sdm_doc}
\usepackage{graphicx,url,natbib,alltt}

\title{The ODI-PPA Calibration Library System}
\author{F. Valdes$^1$, R. Swaters$^1$, Y. Sun$^2$}
\otheraffiliation{
$^1$NOAO Science Data Management, P.O. Box 26732, Tucson, AZ 85732\\
$^2$Data-to-Insight Center, Pervasive Technology Institute, Indiana University, Bloomington, IN
47408
}
%\pubhistory{DRAFT: \today}
\pubhistory{DRAFT: May 11, 2012}
\reportnumber{PL103/CDR-45}
%\reportnumber{CDR-45}
\keywords{ODI, AuCaP, pipeline calibration, data engine, calibration
planner, calibration database}
\runningtitle{ODI-PPA Calibration Library}

\begin{document}
\frontmatter

\tableofcontents
%\newpage 

\listoffigures
%\newpage

\listoftables
\newpage

\mainbody

\begin{abstract}

The ODI-PPA Calibration Library manages calibration file metadata.  It has
two primary functions, ingesting metadata about calibration files and
responding to requests for the best available calibration for individual ODI
exposures.  The system also provides interfaces for pipeline scientists and
operators to manage the metadata such as inserting, deleting, and updating
metadata.  The primary components of the system within the PPA are the data
engine, calibration planner, and calibration database.  The interfaces
described here are the database schema for the calibration database and
VOTables used between the data engine, calibration planner, and workflow
system.

\end{abstract}

\section*{Purposes of this Document}
\addcontentsline{toc}{section}{Purpose of this Document}

This document serves as a design specification for the ODI-PPA
calibration library system.  It includes an overview and
specifications for the calibration library database and the VOTable
interface files.

%\newpage

\section{Overview}

Almost by definition one of the most fundamental aspects of an instrument
calibration pipeline is the application of the most appropriate calibration
algorithms and data to the raw instrument data.  The algorithms are the
provence of the pipeline system and the selection of calibration data is the
provence of the calibration library system.  The purpose of this document is
to describe the latter for the ODI-PPA (One Degree Imager Pipeline, Portal,
and Archive).  As the library metaphor implies, the calibration library
system provides a way to check-in and check-out calibration information.  It
includes a "catalog" for the metadata, "shelves" for storing the resources,
and a "reference librarian" to identify the best resource.

In the ODI-PPA the calibration library system involves the data engine,
DataCapacitor/HPSS, XMCCat, calibration planner, and CalDB components.
There are also user interfaces to allow operators and pipeline scientists to
manage the library such as for inserting, updating, reviewing and deleting
calibration metadata and files.

Figure~\ref{fig:seqoverview} shows a sequence overview of the two main
function of the calibration library system: adding calibration files (aka
PutCal) and selecting them for processing (aka GetCal).  Calibration files,
for example master biases and flat fields, are generally created by the
Automatic Calibration Pipeline (AuCaP) but other sources, typically
involving the instrument and pipeline scientists, are also possible.  In the
PutCal function, the data engine is responsible for receiving the files and
saving them in the PPA archive system.  This is the same process and system
used for all raw and processed data.  Saving files, the "shelving"
metaphor, consists of ingesting metadata into the archive
metadata catalog (XMCCat) and putting the files into the PPA file storage
system (data capacitor and high performance storage system).  For the
calibration library system, an additional step is the ingestion of a small
subset of metadata specific to the calibration files into a calibration
library database (CalDB).  In particular into a table, CalCat, which
corresponds to the calibration library "catalog" alluded to earlier.

The association of calibration files with raw data, for processing by AuCaP,
is done by the calibration planner.  Since the data engine is also the high
level orchestrator for pipeline processing, it is charged with extracting a
table of all the raw exposures to be processed for a particular observing
block.  This table contains file reference URIs as well as all the metadata
needed to select a calibration for each exposure.  The metadata includes
things such as when the exposure was taken and what filter was used.
This table is supplied to the calibration planner to make calibration
assignments.  The calibration planner accesses the calibration catalog to
get metadata for all calibrations which satisfy constraints imposed by the
exposures.  It then applies rules to select the "best" calibration for each
exposure.  The result of the calibration planner is then a table of
exposures to process along with the calibration files associated with the
exposures.  This information is then passed on by the data engine to AuCaP,
through the OGCE workflow engine, for processing.  The "planning" part of
the calibration planner refers to organizing the exposures into a sequence
of workflows to be "orchestrated" by the data engine, which includes the
step of retrieving and staging both the raw exposures and the calibration
files identified by the calibration planner.

There is one aspect of the calibration workflow that merits some discussion.
For some of the workflow steps, such as creating master bias and
dome/twilight flat fields, new calibrations are generated which may
then be applicable to the subsequent steps.  The data engine
orchestration takes this into account by having it archive calibrations
from one step after which the calibration planner is called again to select
calibrations for the next step.  At this point the new calibration will
already be in the calibration system.  This bootstrapping of calibrations is
the reason the calibration planner is not simply called once at the very
beginning with just the list of raw exposures (which include raw calibration
exposures).

There is one subtlety concerning dark sky calibrations.  One of the main
workflows orchestrated by the data engine, the one that calibrates static
on-sky exposures, produces dark sky calibrations from the ensemble of
exposures if the data allows.  These calibrations may then be applied to the
individual exposures.  Rather than break the workflow to return data
products at the point the calibrations are created and before they would be
applied so that the data engine can ingest them and the calibration planner
can select them, the calibration will suggest dark sky calibrations
previously ingested.  The pipeline will then use the same selection logic
discussed in this document to select between the suggested calibrations and
the one generated in the middle of the workflow and apply the result.  Only
at the end of the complete workflow are the new dark sky calibration added
to the calibration library.  This has the same effect as breaking up the
workflow but it is much more efficient in not having to stage the raw data,
retrieve calibrations and the intermediate exposure data, and then restage
these data files for the application of the dark sky calibration in a new
workflow.

One final point to keep in mind in this document is that when we refer to a
file, whether raw or calibration, this means for a single OTA.  So while
conceptually thinks of calibration processing and calibration data in terms
of exposures, the number of files is actually 64 per exposure for the full
focal plane or some lesser number for the partial focal plane.  This means
there will be many records in the calibration database or in the VOTables
for an exposure or calibration.  However, the consistent application of
calibrations requires that when OTA calibrations are selected for an
exposure, they all be from the same parent data.  For example, the selection
of a master dome flat from a number of candidate master dome flats means
that all the OTAs from the same master dome flat (that is derived from the
same dome flat sequence) be applied to the matching OTAs of the exposure
being calibrated.

\begin{figure}[t]
\centering{
\includegraphics[width=4in]{seq.pdf}
%\vskip-5cm
%\includegraphics[width=5.5in]{seq.ps}
\caption{Calibration System Overview\label{fig:seqoverview}}
}
\end{figure}

\section{The Calibration Database}

The library catalog is the heart of the calibration system.  It is a
database table containing metadata about each calibration.  The metadata
fall effectively into three classes; constraints, quality metrics, and
documentation.  The calibration information is inserted into the catalog by
the data engine whenever it receives calibration files.  It harvests the
data from the image headers or (equivalently) from XMCCat, after
archive ingestion, and turns it into insertion records.  Because
the number of fields and number of calibrations is relatively small,
only a single, simple table schema is required.  The database engine used can
also be simple, e.g.  SQLite, though any engine providing APIs may be used.

The proposed schema for the \emph{CalCat} table is shown in
table~\ref{CalCat}.  It is possible that additional fields may be identified
during development but the goal is to not refactor the catalog once it is in
production use.

\begin{table}
\caption{Catalog Fields for Table \emph{CalCat} \label{CalCat}}
\begin{center}
\begin{tabular}{lll}
\textbf{fileref}        &char           &File reference for retrieval\\
\textbf{createtime}     &timestamp      &Calibration creation time\\
\textbf{caltype}        &char           &Calibration type (bias, dflat, etc)\\
\textbf{mjdobs}         &float          &MJD of calibration observation\\
\textbf{dmjdlow}        &float          &Delta MJD for validity window\\
\textbf{dmjdhigh}       &float          &Delta MJD for validity window\\
\textbf{otaid}          &char           &OTA identifier\\
\textbf{ccdbin1}        &int            &Binning factor\\
\textbf{ccdbin2}        &int            &Binning factor\\
\textbf{filter}         &char           &Filter identifier\\
\textbf{exptime}        &float          &Exposure time\\
\textbf{snr}            &float          &Avg signal-to-noise ratio or sigma\\
\textbf{ncombine}       &int            &Number of exposures used\\
\textbf{refcomp}        &float          &Reference comparison metric\\
\textbf{skycover}       &float          &Sky coverage metric in night stacks\\
\textbf{plflag}         &float          &Pipeline quality flag\\
\textbf{opflag}         &float          &Operator/scientist quality flag
\end{tabular}
\end{center}
\end{table}

\begin{description}

\item{\textbf{fileref}\\}
    The calibration datum the catalog is designed to provide is the
    file reference field.  This should be a URI, which can be
    resolved by a storage system such as irods, rather than a physical
    path to allow the file to migrate.

\item{\textbf{createtime}\\}
    A timestamp for when the calibration was created.  This is not
    used for the automatic selection of a calibration but it is useful
    documentation and potentially of use for operator/scientist
    purposes.

\item{\textbf{caltype}\\}
    The calibration type is one of:\\
        \begin{tabular}{rcl}
        \emph{bias}&-&Bias\\
        \emph{dark}&-&Dark of a particular exposure time\\
        \emph{dflat}&-&Dome flat field\\
        \emph{tflat}&-&Twilight flat field\\
        \emph{fringe}&-&Fringe template\\
        \emph{iflat}&-&Illumination flat field\\
        \emph{refbias}&-&Reference bias\\
        \emph{refdflat}&-&Reference dome flat field\\
        \emph{reffringe}&-&Reference fringe template\\
        \emph{refiflat}&-&Reference illumination flat field\\
        \emph{refskymap}&-&Reference sky map
        \end{tabular}

    The reference calibrations are used during creation of new
    calibrations to identify unexpected changes.  These files may
    in a format used for the comparison; for instance, binned and
    smoothed to a smaller size.

\item{\textbf{mjdobs}\\}
    The effective modified Julian Date of the data producing the
    calibration.  This is useful documentary information but it is important
    in the calibration evaluation process as the fiducial point for the
    window of validity for the calibration.

\item{\textbf{mjdlow}\\}
    This is a value to be \emph{subtracted} from the mjdobs value to define
    a limit for the the window of validity.  It is in decimal days. It is
    legal, though very unusual, for the value to be negative. It is
    often the same as the high value to define a centered window
    around the date of observation.  It may also be zero to define a
    validity window strictly after the calibration data was taken.

\item{\textbf{mjdhigh}\\}
    This is a value to be \emph{added} to the mjdobs value to define
    a limit for the window of validity.  It is in decimal days. It is
    legal, though very unusual, for the value to be negative. It is
    often the same as the low value to define a centered window
    around the date of observation.

\item{\textbf{otaid}\\}
    The OTA identifier used to match calibration files to the
    appropriate data OTA.

\item{\textbf{ccdbin1}, \textbf{ccdbin2}\\}
    The binning factors along the detector axes (1=serial direction,
    2=parallel direction).  Generally calibrations are matched to the
    same binning factors as the data being calibrated.

\item{\textbf{filter}\\}
    The filter identifier which must be one of the standardized
    strings.  This is used for those calibrations taken with the
    shutter open.

\item{\textbf{exptime}\\}
    The exposure time associated with the calibration.  Generally this
    only applies to dark calibrations and the time might be the
    effective dark time though the difference is typically
    insignificant.

\item{\textbf{snr}\\}
    The signal-to-noise or statistical weight (inverse variance) of pixels
    in the calibration is a fairly direct indicator of the quality of the
    calibration.  This is a single metric of the typical value estimated as
    an average over all the pixels in the calibration file.  This is the
    most important metric since the goal of applying a calibration is to
    introduce as little noise as possible.  A signal-to-noise value
    is used for flat fields and a statistical weight is used for bias
    and dark exposures.

\item{\textbf{ncombine}\\}
    Most calibrations are derived by combining individual exposures.
    The field records the number of exposures used in creating the
    calibration.  A larger number is often an indicator of a better
    calibration.

\item{\textbf{refcomp}\\}
    Calibrations are compared to other, "reference", calibrations
    and this quantity is a measure of the similarity.  A high degree
    of similarity is an indicator of a good calibration.

\item{\textbf{skycoverage}\\}
    Calibrations created from on-sky exposures depend on sources not
    covering a particular pixel in every exposure.  In other words, a pixel
    needs to sample "blank" sky in some exposures.  This metric provides an
    indicator of how well sky is sampled over the pixels in the calibration.
    Better sky coverage is an indicator of better on-sky derived
    calibrations.

\item{\textbf{plflag}\\}
    A numeric data quality flag provided by the pipeline.  The flag is
    defined such that a higher value is a better choice.  A value
    of zero indicates the pipeline vetoes this calibration.

\item{\textbf{opflag}\\}
    A numeric data quality flag provided by the pipeline operator or
    scientist. The flag is defined such that a higher value is better
    choice.  Calibrations will normally not be graded by a person
    and will have a default value of 1.  A value of zero indicates the
    operator vetoes this calibration.  A value greater then one is a
    way to promote use of particular calibrations.  There may be other
    operator mechanisms which override the calibration library to allow
    explicit use of a particular calibration.

\end{description}


As noted in the overview, a calibration "exposure" is actually
composed of some number of OTA files.  In order for the selection
process to be able to chose all OTAs from a calibration exposure
as a unit the metadata for all OTAs must be the same.  AuCaP, when it
creates calibrations is required to do this.  So a quantity like the
SNR is not only some composite over all pixels in an OTA it must also
be a composite over all OTAs.

\section{The Data Engine}

The data engine is a central component of the PPA.  The full
functionality of this component is outside the scope of this document.
Here we describe the functions of the data engine which are part of
the calibration library system.

Adding calibrations to the system consists of archiving them, just as
with all data products, and inserting metadata extracted from the
files into the calibration database.  This was described in the
overview.  This step occurs whenever new calibrations are available.
This is normally as the result of a workflow completing.  However,
it can also be through a operator initiated submission where the
calibrations are produced, or improved, by someone such as an
instrument or pipeline scientist.  Another operator function which
is related is to cause the calibration database to be modified
(updates or deletions).  Typically, this consists of changing the
operator flag to upgrade or downgrade the use of a particular
calibration.

The process of assigning calibrations to raw exposures was also outlined
earlier.  The starting point is a list of exposures.  In the larger
context of the PPA workflow the list is first defined as exposures
from a set of nights.  This is then grouped into sublists associated with
a sequence of workflows by the calibration planner.

When the initial request to process a set of nights is received the data
engine resolves this into a table of exposures.  The table consists of the
file references and calibration database query metadata.  The information is
extracted from XMCCat.  The table is constructed as a VOTable as described
in more detail in section~\ref{VOTables}.

The data engine executes the calibration planner component with the VOTable
as input.  The output is another VOTable file containing two tables, again
as described in \S\ref{VOTables}.  The first table, called the
workflow table, includes a parameter defining the workflow to be executed
and an association table with the file references for raw exposure to be
processed by the workflow and the file references for the calibrations
assigned to that exposure.  The second table, called the remainder table, is
simply the subset of the input table not contained in the workflow table.
The remainder table is used as input to the calibration planner after the
specified workflow is completed and any calibrations produced are archived,
thus starting the next cycle in the orchestration of a sequence of
workflows.  The remainder table will be absent when the workflow table for
the last workflow is produced.

The data engine and/or the workflow engine need to read the workflow table
and find all the unique files referenced (since one calibration file is
typically associated with many raw exposures) and handle the staging of the
data for the workflow.  This involves retrieving the actual files from the
PPA archive and placing them where the workflow code will access them.  The
workflow table is ultimately passed on as input to the requested workflow.
If the file references need to be expanded for the workflow, i.e.
dereferenced into actual pathnames, the table would need to be transformed.


\section{The Calibration Planner}

The calibration planner has two roles, organizing exposures into groups and
directing the data engine as to which workflow to run for each group and
assigning the best calibration files for each exposure from those available
in the calibration database.  It does this based on metadata for the raw
exposures to be processed.  It is called by the data engine after each
workflow is completed so as to have access to any calibrations produced and
stored by the data engine.

The input is a VOTable containing a table listing the file references
of files to be processed and metadata for the exposures needed to
assign calibrations.  It also may contain a parameter identifying the
last workflow completed.

The planner groups the exposures by observation type; such as bias,
dark, dome flat, static sky, and guided sky exposures.  From this it
determines the next workflow needed.  For that group it uses the
metadata to locate potential calibrations and then assign the best one
to each exposure.

As just implied, there are two steps to assigning calibrations to an
exposure.  The first step is a query to the calibration
database with constraints based on the metadata for the exposure and
some system parameters.  The query returns zero or more calibrations
satisfying the constraints along with data quality metrics.  If there
are no calibration then a potential error action may be taken or
there will simply be no calibration assigned and the response deferred
to workflow to decide what to do.

The second step when there are multiple candidates is to rank them
based on the data quality metrics.  The way the metrics are combined
is an algorithmic detail.  The highest ranked calibration is then
chosen as the "best" calibration and assigned to the exposure.

The raw exposure file references and the assigned calibrations for
the exposure form a table row.  The table is the group of exposures
that is to be processed by the next workflow.  The table is formatted
as a resource in a VOTable.  All the exposures not in the current
group to be processed are added to the VOTable as a second resource.
The data engine will separate the tables into one for the workflow and
one for input to the next execution of the calibration planner.

\subsection{The Calibration Database Query}

The assignment of calibration files to raw exposures (by OTA) is done by the
calibration planner component.  As part of this the planner queries the
CalCat catalog for calibrations which meet constraints imposed by both the
target raw exposures and system parameters.  Table~\ref{tbl:expmetadata}
shows the target metadata used in queries and supplied to the calibration
planner in a VOTable input by the data engine.  Table~\ref{tbl:parameters}
presents calibration system parameters provided as part of the PPA parameter
mechanism.

\begin{table}[ht]
\caption{Target Exposure Metadata\label{tbl:expmetadata}}
\begin{center}
\begin{tabular}{|rcl|}
\hline
\textbf{obstype}&-&observation type\\
\textbf{mjdobs}&-&observation MJD\\
\textbf{otaid}&-&OTA identifier\\
\textbf{ccdbin1}&-&serial binning\\
\textbf{ccdbin2}&-&parallel binning\\
\textbf{filter}&-&filter identifier\\
\textbf{exptime}&-&exposure time\\
\hline
\end{tabular}
\end{center}
\end{table}

\begin{table}[ht]
\caption{Calibration Query Parameters\label{tbl:parameters}}
\begin{center}
\begin{tabular}{|rcl|}
\hline
\textbf{opflag}&-&minimum value for operator flag\\
\textbf{dmjd}&-&MDJ half-window\\
\textbf{dexptime}&-&exposure time percentage window\\
\hline
\end{tabular}
\end{center}
\end{table}

\begin{description}
\item{\textbf{mjdobs}\\}
    The observation MJD of the target exposure.
\item{\textbf{otaid}\\}
    The OTA identifier of the piece of the target exposure.
\item{\textbf{ccdbin1}, \textbf{ccdbin2}\\}
    The binning used for reading out the target exposure.
\item{\textbf{filter}\\}
    The filter identifier of the target exposure.
\item{\textbf{exptime}\\}
    The exposure time of the target exposure.
\item{\textbf{opflag}\\}
    The minimum acceptable operator flag.
\item{\textbf{caltype}\\}
    The calibration types required.  This is determine based on the
    observation type of the target exposure.  For example, a dome
    flat field calibration is required for a science exposure but not a
    dome flat field sequence exposure.
\item{\textbf{dmjd}\\}
    The acceptable window about the target exposure's observation MJD.
    Note that this is different than the window of validity of a
    calibration.  Calibrations selected must satisfy both of these
    windows.
\item{\textbf{dexptime}\\}
    The acceptable window of calibration exposure times relative to
    the target's exposure time.  This is expressed as a fraction of
    the target's exposure time; e.g. 10\%.  This is comes into play
    for finding a dark calibration exposure for a target exposure which was
    not taken with exactly the same exposure time.
\end{description}

The calibration planner is free to define the query to the calibration
database as is required.  For example whether a constraint clause is needed
for a particular type of metadata or whether the constrain expression is an
equality, list selection, or pattern.  Figure~\ref{fig:query} shows a
schematic query using all the constraints.  In the figure those quantities
in brackets are the exposure and parameter values defined earlier.  In
addition [caltypes] is a list of required calibrations types defined in the
context of the exposure. Note that the details of the syntax may vary
depending on the database engine adopted.

\begin{figure}[ht]
\caption{Schematic query to select a calibration\label{fig:query}}
\footnotesize
\begin{verbatim}
    SELECT fileref, snr, ncombine, refcomp, skycover,
        abs([mjdobs]-mjdobs) as dmjd, abs([exptime]-exptime) as dt
    FROM CalCat
    WHERE caltype in ([caltypes])
    AND (mjdobs-[mjdobs]) <= min(dmjdlow,[dmjd])
    AND ([mjdobs]-mjdobs) <= min(dmjdhigh,[dmjd])
    AND otaid = [otaid]
    AND filter = [filter]
    AND dt <= ([dexptime]*[exptime]/100)
    AND plflag > 0.
    AND opflag >= [opflag]
\end{verbatim}
\end{figure}

\subsection{Calibration Selection}

The selection of the "best" calibration for a particular exposure is based
on data quality flags and metrics retrieved from the calibration database.
This selection can be made as elaborate as desired using A.I. or other
decision methodologies.  The complication is that when there are multiple
data quality parameters and metrics it is challenging to incorporate
them into a selection criterion.  For example, how important is proximity
in time relative to the signal-to-noise.

One of the design strategies for the PPA calibration system is to isolate
the calibration selection decision to a single component, namely the
calibration planner.  This isolation and modularity allows evolution of the
decision logic.  We start with a simple method based on the operator
flag, the signal-to-noise / statistical weight metric, and proximity
in time.  The calibrations which satisfied the constraint selection
are ordered by OPFLAG, then by SNR, and finally by the difference in
MJD between the calibration and target exposure.  From this ranking the
top choice is selected.  In words this means that the highest
operator flag value is chosen but if these are equal, the usual default
case, the best signal-to-noise value is chosen and, in the even of
nearly equal SNR then the closest in time is selected.

In order for the time proximity to apply the SNR metric has to be
transformed into discrete values.  This is done by rounding the
logarithm of the SNR, digitizing into discrete values of some
accuracy, say 10\%, and applying a ceiling value.  That is, values
above some threshold are treated equally and below that threshold
values that are within 10\% are considered comparable.  This logic
is similar to one used with the NOAO pipelines except that instead of
the discretized SNR the number of combined exposures is used, again
with a ceiling.

So why are there more metrics included in the calibration library and why
are they not used?  Firstly, by recording these parameters the possibility
of making use of them in the future is preserved.  Secondly, we are actually
making use of some of them through the constraint selection.  The key
parameter here is the pipeline flag.  This parameter is defined to allow the
pipeline to veto a calibration, using a value of zero, which is enforced
through the constraint query.  The pipeline vetoes a calibration itself
based on the metrics of similarity to a reference and on the evaluation sky
coverage for dark sky generated calibrations.

Returning again to the subject of exposures vs. OTAs, it was noted
earlier that the metadata used in the selection must be the same for
all OTAs from a particular calibration exposure.  This is expected to
be enforced by the pipeline that generates the calibration.  However,
if this is not the case the calibration planner can be responsible for
making a composite value (e.g. an average over all the OTAs).  But in
addition, the calibration planner has to ensure that when a
OTA calibration is assigned to one OTA of an exposure, all OTAs of the
exposure are assigned corresponding OTAs from the same calibration.

\section{The VOTables\label{VOTables}}

The interface between the data engine and the calibration planner, as well
as the subsequent input to the workflow from the data engine, are VOTables
(\citep{VOTables}).  These VOTables all have the same basic structure
consisting of one or more resources each containing one or two tables.  The
tables in a resource are of two types; a table used as input to a workflow
and a table used as input to the calibration planner.

Figures~\ref{VOTable1}~to~\ref{VOTable5} illustrate the form of the
files for the initial call to the calibration planner
(fig.~\ref{VOTable1}) and the tables returned by the calibration
planner at each stage of the workflow sequence.  The example content
follows the expected conventions for dataset names, workflow names,
raw exposure filenames, and calibration filenames.  But the important
specification are in the tags and attributes.  Below are details on
some of the key tags and attributes.

\begin{description}
\item{\textbf{utype}\\}
    The "utype" (usage-specific type) is provided to identify the type
    of resource or table without requiring the consumer to parse the
    content.  The utypes are defined in the following descriptions.
\item{\textbf{name} (RESOURCE and TABLE)\\}
    The "name" attribute in the RESOURCE and TABLE tags provide a
    unique name for each of these elements.  The names are the same
    as "dataset" names used inn the workflow processing.  These names
    are built from the following pieces: 
    {\small\begin{verbatim}
    <semester>_<start date>_<process id>[-<workflow>[<filter>]]
    \end{verbatim}}
    \begin{tabular}{rcl}
    \texttt{semester}&-&semester as 2 digit year and A or B\\
    \texttt{start date}&-&calendar starting data of block\\
    \texttt{process id}&-&process id formed as a hex time stamp\\
    \texttt{workflow}&-&one of ftrB, ftrF, ftrS, ftrG\\
    \texttt{filter}&-&filter abbreviation for filter dependent workflows
    \end{tabular}
\item{\textbf{RESOURCE}\\}
    A resource has a "name" and a "utype".  The name is a unique
    dataset identifier.  The utype is one of:\\
    \begin{tabular}{rcl}
    \texttt{A}&-&initial input exposure data for a block of nights\\
    \texttt{B}&-&output for bias workflow\\
    \texttt{F[filter]}&-&output for flat field workflow for a filter\\
    \texttt{S[filter]}&-&output for static science exposure workflow for a filter\\
    \texttt{G[filter]}&-&output for guided science exposure workflow for a filter
    \end{tabular}
\item{\textbf{TABLE}\\}
    A table has a "name" and a "utype".  The name is a unique dataset
    identifier matching.  It is the same as the resource name.  The
    utype is one of:
    \begin{description}
    \item{\texttt{WorkflowTable -}} table defining data for
    a workflow
    \item{\texttt{RemainderTable}} table of exposure data for
    the next call to the calibration planner
    \end{description}
    These are useful for the data engine to separating these tables when
    handling them.  Note there is no workflow table in the initial
    file for the calibration planner and there is
    no remainder table when the workflow requested is the last one
    for some subset of data.
\item{\textbf{RemainderTable}\\}
    A remainder table has a standard set of parameters and columns.
    The filename is the URI to be used by the data engine to setup
    data for the workflow engine.  The remaining columns are metadata
    used for calibration selection by the calibration planner.
\item{\textbf{WorkflowTable}\\}
    The workflow tables vary in the number of columns.  There is a
    parameter specifying the workflow that it applies to.  The columns
    are the filename of the raw exposure to be processed and then
    file references for calibrations associated with the exposure.
    Since different workflows require different sets of calibration
    files the number of columns vary by the target workflow. Note
    that some workflows in the standard sequence of workflows may be
    skipped because of lack of data.  So the data engine should not
    depend on a hardwired sequence of workflows.
\end{description}

As noted in the overview, the number of table rows for the raw
exposures and for the calibrations is based on the number of OTAs.
The examples show a single OTA for a single exposure or calibration.


\begin{figure}[p]
\caption{Initial Raw Exposure VOTable\label{VOTable1}}
\scriptsize
\begin{alltt}
\include{VOTables/votable1}
\end{alltt}
\end{figure}


\begin{figure}[p]
\caption{Bias Output VOTable\label{VOTable2}}
\scriptsize
\begin{alltt}
\include{VOTables/votable2}
\end{alltt}
\end{figure}

\begin{figure}[p]
\caption{Flat Output VOTable\label{VOTable3}}
\scriptsize
\begin{alltt}
\include{VOTables/votable3}
\end{alltt}
\end{figure}

\begin{figure}[p]
\caption{Static Exposure Output VOTable\label{VOTable4}}
\tiny
\begin{alltt}
\include{VOTables/votable4}
\end{alltt}
\end{figure}

\begin{figure}[p]
\caption{Guided Exposure Output VOTable\label{VOTable5}}
\scriptsize
\begin{alltt}
\include{VOTables/votable5}
\end{alltt}
\end{figure}

% Use bib files if possible.
\bibliography{PL103,PLSeries}
\bibliographystyle{abbrv}

% Otherwise use:
%\begin{thebibliography}{}
%\bibitem[Author(DATE)]{REF} Author, A., et al., DATE, PUB, 1234, 56
%\end{thebibliography} 

\end{document}
