\contentsline {section}{Purpose of this Document}{1}
\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Summary of Data Flow through AuCaP}{1}
\contentsline {section}{\numberline {3}Calibration Data}{2}
\contentsline {subsection}{\numberline {3.1}Calibration Library}{2}
\contentsline {subsection}{\numberline {3.2}Creating Basic Calibrations}{3}
\contentsline {section}{\numberline {4}Processing of the Static Exposures}{4}
\contentsline {section}{\numberline {5}Deriving Advanced Calibration Data from Static Exposures}{5}
\contentsline {section}{\numberline {6}Calibration of Coherent and Locally Guided Data}{6}
\contentsline {section}{\numberline {7}Resampling and Stacking}{7}
\contentsline {section}{\numberline {8}Automated Calibration Pipeline Software}{8}
\contentsline {section}{\numberline {9}Documentation}{8}
\contentsline {section}{\numberline {10}References}{8}
\contentsline {section}{\numberline {11}Requirements and Derived Requirements from the PASRD}{9}
\contentsline {section}{\numberline {12}Feedback from Use Case Workshops}{16}
