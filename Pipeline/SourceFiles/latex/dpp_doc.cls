%%
%% SDM Document Style File.
%%
%% --------------------- identification --------------------------
\NeedsTeXFormat{LaTeX2e}%
\ProvidesClass{dpp_doc}[2006/10/30 SDM Document Class]%
%% -------------------- initializations --------------------------
\RequirePackage{ifthen}
\newboolean{nofrontmatter}	% Skip all front material.
%% ------------------- option declarations -----------------------
\DeclareOption{phd}{\setboolean{phd}{true}}
\DeclareOption{nofrontmatter}{\setboolean{nofrontmatter}{true}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
%% ------------------- option parsing ----------------------------
\ProcessOptions\relax
%% ----------------- package loading -----------------------------
\LoadClass{article}%
\RequirePackage{fancyhhdr}
\RequirePackage{times}
%% ------------------ main code ----------------------------------

\renewcommand{\contentsname}{Table of Contents}

%% margin definitions.

\newlength{\tmarg}\setlength{\tmarg}{1.0in}	% top margin
\newlength{\bmarg}\setlength{\bmarg}{1.0in}	% bottom margin
\newlength{\lmarg}\setlength{\lmarg}{1.0in}	% left margin
\newlength{\rmarg}\setlength{\rmarg}{1.0in}	% right margin

\setlength{\topmargin}{-1.0in}
\addtolength{\topmargin}{\tmarg}

\setlength{\oddsidemargin}{-1.0in}
\addtolength{\oddsidemargin}{\lmarg}

\setlength{\evensidemargin}{-1.0in}
\addtolength{\evensidemargin}{\rmarg}

\setlength{\textwidth}{\paperwidth}
\addtolength{\textwidth}{-\lmarg}
\addtolength{\textwidth}{-\rmarg}

\setlength{\textheight}{\paperheight}
\addtolength{\textheight}{-\tmarg}
\addtolength{\textheight}{-\headheight}
\addtolength{\textheight}{-\bmarg}
\addtolength{\textheight}{-\footskip}

\pagestyle{fancy}
\lhead{{\bf {\@runningtitle}}}
\rhead{{\bf {\@runningdoc \@reportnumber}}}
\chead{}
\lfoot{}
\cfoot{}
\rfoot{\thepage}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}


%% following information prints out all front matter which 
%% presently includes:
%%	-- title page which meets our format guide lines.

\newcommand{\frontmatter}{%
    \ifthenelse{\boolean{nofrontmatter}}{}%
    {%
	\setcounter{page}{1}
	\pagenumbering{roman}
        \titlepg
         %\dppdocreppg
       	%\historypage
    }%
    \raggedbottom
}%
 
\newcommand{\mainbody}{%
	\newpage
	\setcounter{page}{1}
	\pagenumbering{arabic}
	\raggedbottom
}%

\renewenvironment{abstract}{%
    \begin{center}
	{\bf {\Large Abstract}}\par%
    \end{center}
    }{\par \vspace*{0.5in}
    \noindent
    {\bf Keywords:} \@keywords
    \newpage}%

%% following is the definition of the title page.

\def\@title{}% default is nothing
\def\@author{}% default is nothing
\def\pubhistory#1{\gdef\@pubhistory{#1}}
\def\@pubhistory{}% default is nothing
\def\otheraffiliation#1{\gdef\@otheraffiliation{#1}}
\def\@otheraffiliation{}% default is nothing
\def\reportnumber#1{\gdef\@reportnumber{#1}}
\def\@reportnumber{}% default is nothing.
\def\submitdate#1{\gdef\@submitdate{#1}}
\def\@submitdate{\number\the\year/\number\the\month/\number\the\day}
\def\keywords#1{\gdef\@keywords{#1}}
\def\@keywords{}% default is nothing

\def\runningtitle#1{\gdef\@runningtitle{#1}}
\def\@runningtitle{}% default is nothing
\def\runningdoc#1{\gdef\@runningdoc{#1}}
\def\@runningdoc{}%

\newcommand{\titlepg}{%
    \thispagestyle{empty}
    \begin{flushleft}
	{\bf {NOAO SDM Document \@reportnumber}}\par%
	\underline{\hspace*{\textwidth}}\par%
	\vspace*{\stretch{1}}%
%	{\bf {\Large \@title}}\par%
%	\vspace*{\stretch{1}}%
%	{\bf {\begin{tabular}[t]{@{}l@{}}\@author\end{tabular}}}\par
  \begin{center}%
    {\LARGE \@title \par}%
    \vskip 3em%
    {\large
     \lineskip .75em%
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}\par}%
      \vskip 1.5em%
%    {\large \@date \par}%       % Set date in \large size.
  \end{center}\par
  \@thanks
  \vfil\null
	\vspace*{\stretch{1}}%
	\begin{center}
	{\bf  National Optical Astronomy Observatories\\
                 Science Data Management}
	\end{center}
	\par%
	\vspace*{\stretch{1}}%
	\begin{center}
	 \@pubhistory
	\end{center}
	\vspace*{\stretch{5}}%
	{\footnotesize \@otheraffiliation}\par%
	\underline{\hspace*{\textwidth}}\par%
	Copyright \copyright \space \number\the\year ~ by the authors.
%	Copyright retained by the authors.  This report may be
%	used, copied and distributed in its entirety for academic purposes.  Any 
%	copies must include this cover page and notice.  Any non-academic use
%	requires the permission of the authors.
    \end{flushleft}
    \newpage
}%

%% heather's attempts
\newcommand{\dppdocreppg}{%
    \thispagestyle{empty}
    \begin{flushleft}
    {\bf {\Large NOAO SDM Document}}\par%
    \end{flushleft}
    \vspace{0.25in}%
    This document series allows affiliates of the NOAO to
    publish detailed and recent research results in a timely manner. 
    It is {\em not} intended that these documents duplicate outside 
    publications.  

%     \vspace{0.25in}
%     \noindent
%     For more information about this document series, please
%     contact:\\ Irene Barg {\tt ibarg@noao.edu}.
%% end heather
    \newpage
}%

\newcommand{\historypage}{%
    \thispagestyle{empty}
    \begin{flushleft}
    {\bf {\Large Publication History}}\par%
    \end{flushleft}
    \vspace{0.25in}%
    \begin{flushleft}
    {\@pubhistory}\par%
    \end{flushleft}
    \vspace*{\stretch{9}}%
    \begin{flushleft}
    \copyright \number\the\year ~ by the authors.\par%
    \end{flushleft}
    \vspace*{0.25in}%
    \newpage
    \setcounter{page}{2}
    \vspace*{1in}
    \begin{center}
    \textbf{\Large Disclaimer}\\
    \vspace{-6pt}
    \rule{1.2in}{1mm} \\
    \bigskip
\begin{minipage}[h]{4.1in}
{\small
The documents are subject to internal review by
the National Optical Astronomy Observatories (NOAO) 
Science Data Management (SDM). However, the material,
as presented, does not necessarily reflect any position of the NOAO. 
Further, neither NOAO, nor the SDM, makes any warranty or assumes any legal
liability or responsibility for the accuracy, completeness, or usefulness of any 
information contained in this report.
}
\end{minipage}

\vspace{8pt}

\begin{minipage}[h]{4.1in}
{\small
NOAO is operated by the Association of Universities for Research in
Astronomy (AURA), Inc., under cooperative agreement with the 
National Science Foundation.
}
\end{minipage}

\vspace{8pt}

    \rule{4.8in}{1.5mm} \\
    \bigskip
{\small
    \begin{tabular}{ll}
    Elizabeth Stobie & Manager, Science Data Management, NOAO SDM
    \end{tabular}
}
    \end{center}
    \newpage
}%

\newcommand{\copyrightnotice}{%
	{\tiny
	}%
}%

% font sizing, taken from /usr/local/lib/tex/inputs/size12.clo

\renewcommand\normalsize{%
   \@setfontsize\normalsize\@xiipt{14.5}%
   \abovedisplayskip 12\p@ \@plus3\p@ \@minus7\p@
   \abovedisplayshortskip \z@ \@plus3\p@
   \belowdisplayshortskip 6.5\p@ \@plus3.5\p@ \@minus3\p@
   \belowdisplayskip \abovedisplayskip
   \let\@listi\@listI}
\normalsize
\renewcommand\small{%
   \@setfontsize\small\@xipt{13.6}%
   \abovedisplayskip 11\p@ \@plus3\p@ \@minus6\p@
   \abovedisplayshortskip \z@ \@plus3\p@
   \belowdisplayshortskip 6.5\p@ \@plus3.5\p@ \@minus3\p@
   \def\@listi{\leftmargin\leftmargini
               \topsep 9\p@ \@plus3\p@ \@minus5\p@
               \parsep 4.5\p@ \@plus2\p@ \@minus\p@
               \itemsep \parsep}%
   \belowdisplayskip \abovedisplayskip
}
\renewcommand\footnotesize{%
   \@setfontsize\footnotesize\@xpt\@xiipt
   \abovedisplayskip 10\p@ \@plus2\p@ \@minus5\p@
   \abovedisplayshortskip \z@ \@plus3\p@
   \belowdisplayshortskip 6\p@ \@plus3\p@ \@minus3\p@
   \def\@listi{\leftmargin\leftmargini
               \topsep 6\p@ \@plus2\p@ \@minus2\p@
               \parsep 3\p@ \@plus2\p@ \@minus\p@
               \itemsep \parsep}%
   \belowdisplayskip \abovedisplayskip
}
\renewcommand\scriptsize{\@setfontsize\scriptsize\@viiipt{9.5}}
\renewcommand\tiny{\@setfontsize\tiny\@vipt\@viipt}
\renewcommand\large{\@setfontsize\large\@xivpt{18}}
\renewcommand\Large{\@setfontsize\Large\@xviipt{22}}
\renewcommand\LARGE{\@setfontsize\LARGE\@xxpt{25}}
\renewcommand\huge{\@setfontsize\huge\@xxvpt{30}}
\let\Huge=\huge
