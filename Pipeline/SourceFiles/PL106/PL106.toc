\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Data Product Standards}{1}
\contentsline {section}{\numberline {3}Pipeline-Generated Metadata}{1}
\contentsline {subsection}{\numberline {3.1}Keywords Added or Updated by the Pipeline}{3}
\contentsline {subsection}{\numberline {3.2}Observation Type and Data Product Classification}{3}
\contentsline {subsection}{\numberline {3.3}Provenance Metadata}{3}
\contentsline {subsection}{\numberline {3.4}Associations between Data Products}{3}
\contentsline {subsection}{\numberline {3.5}Other Keywords}{6}
\contentsline {subsection}{\numberline {3.6}Metadata for Graphics Files}{6}
\contentsline {section}{\numberline {4}Data Product Description}{6}
\contentsline {subsection}{\numberline {4.1}Image Data}{6}
\contentsline {subsubsection}{\numberline {4.1.1}Calibration Data}{6}
\contentsline {subsubsection}{\numberline {4.1.2}Detrended data}{7}
\contentsline {subsubsection}{\numberline {4.1.3}Reprojected and Stacked Data}{7}
\contentsline {subsection}{\numberline {4.2}Data Quality Maps}{7}
\contentsline {subsection}{\numberline {4.3}Weight Maps}{7}
\contentsline {subsection}{\numberline {4.4}Coverage Map}{7}
\contentsline {subsection}{\numberline {4.5}Catalogs}{7}
\contentsline {subsection}{\numberline {4.6}Graphics Files}{8}
\contentsline {subsection}{\numberline {4.7}Log Files}{8}
\contentsline {section}{\numberline {5}References}{8}
